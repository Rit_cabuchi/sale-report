const state = {
    layout: 'default-layout'
}
const actions = {
    setLayout({ commit }, layout) {
        commit('SET_LAYOUT', layout)
    }
}
const mutations = {
    SET_LAYOUT(state, payload) {
        state.layout = payload
    }
}
export const Layout_store = {
    namespaced: true,
    state,
    actions,
    mutations
}