import Vue from 'vue'
import Vuex from 'vuex'
import { Layout_store } from './Layout_store'
Vue.use(Vuex)

export default new Vuex.Store({
    modules: {
        Layout_store
    }
})